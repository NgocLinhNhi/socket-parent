package heaven.sky.project.socket.server.command_impl;

import heaven.sky.project.socket.server.config.ServerComponentIBuildHandler;
import heaven.sky.project.socket.config.interfaces.ICommand;

//Command Design pattern call Template method như 1 Interface mô tả business
public class ChatServer implements ICommand {

    private ServerComponentIBuildHandler buildComponent;

    public ChatServer(ServerComponentIBuildHandler buildComponent) {
        this.buildComponent = buildComponent;
    }

    @Override
    public void handleMessage() {
        buildComponent.handle();
    }

}
