package heaven.sky.project.socket.server.config;

import heaven.sky.project.socket.config.abstract_template.ComponentBuildHandler;

//Template method design pattern
public class ServerComponentIBuildHandler extends ComponentBuildHandler {

    private static ServerComponentIBuildHandler INSTANCE;

    public static ServerComponentIBuildHandler getInstance() {
        if (INSTANCE == null) INSTANCE = new ServerComponentIBuildHandler();
        return INSTANCE;
    }

    @Override
    protected void preHandle() {
        InitServerSwing.getInstance().buildForm();
    }

    @Override
    protected void doHandle() {
        InitServerSwing.getInstance().addEventFunction();
    }

    @Override
    protected void postHandle() {
        InitServerSwing.getInstance().setSizeSwing();
    }
}
