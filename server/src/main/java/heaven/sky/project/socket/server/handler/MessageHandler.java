package heaven.sky.project.socket.server.handler;

import heaven.sky.project.socket.config.config.SocketServerConfig;
import heaven.sky.project.socket.config.interfaces.IMessage;
import heaven.sky.project.socket.server.config.InitServerSwing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static heaven.sky.project.socket.config.constant.SocketConstant.*;

public class MessageHandler implements IMessage {
    private final Logger logger = LoggerFactory.getLogger(MessageHandler.class);

    private ObjectInputStream ois = null;
    private ObjectOutputStream oos = null;
    private SocketServerConfig instance = SocketServerConfig.getInstance();

    public static MessageHandler getInstance() {
        return new MessageHandler();
    }

    @Override
    public void handleText() {
        InitServerSwing instance = InitServerSwing.getInstance();
        try {
            String data = instance.getTextField().getText();
            oos.writeObject(data);

            instance.getTxArea().append(SERVER + data + "\r\n");
            instance.getTextField().setText(null);
        } catch (IOException ex) {
            logger.error("Handle Text has error : {1}", ex);
        }
    }

    @Override
    public void receiveMessage(Socket communicative) throws IOException {
        ois = new ObjectInputStream(communicative.getInputStream());
    }

    @Override
    public void sendMessage(Socket communicative) throws IOException {
        oos = new ObjectOutputStream(communicative.getOutputStream());
        oos.flush();
    }

    @Override
    public void handleMessageFromClient() throws IOException, ClassNotFoundException {
        while (true) {
            String data = (String) ois.readObject();
            InitServerSwing.getInstance().getTxArea().append(CLIENT + data + "\r\n");
        }
    }

    @Override
    public void handleMessage() {
        try {
            Socket communicative = getConnection();

            receiveMessage(communicative);
            sendMessage(communicative);
            handleMessageFromClient();
        } catch (Exception ex) {
            //Vì Socket là 1 giao thức mạng 2 chiều song song -> chỉ cần 1 client/server close => bên còn lại sẽ nhận được ngay error
            //java.net.SocketException: Connection reset => 1 lỗi khá nỗi tiềng của socket
            //=> Với server chỉ việc đóng server socket và khởi tạo lại giao thức cho server-socket là ok .
            closeIO();
            logger.info("File I/O Has been closed");
            //Cách 1 dùng cho nhiều bài toán là khởi tạo lại socket server
            restartServerSocket(instance);
            logger.error("handle Message Server has error {1}", ex);
        }
    }

    @Override
    public void handleExit() {
        int flag = JOptionPane.showConfirmDialog(null, ASK);
        if (flag == JOptionPane.YES_OPTION) System.exit(0);
    }

    private Socket getConnection() throws Exception {
        instance.initSocketServerConfig();
        return instance.getCommunicative();
    }

    private void restartServerSocket(SocketServerConfig instance) {
        try {
            // Stop Socket server cũ để không  bị bug gọi đệ quy
            instance.stop();
            logger.info("Socked Client has been closed => Auto create new Connection for Server has Done !!!");
            // Sau khi Client đóng Server sẽ tự mở lại kết nối với 1 Client khác với port mặc định
            handleMessage();
        } catch (Exception e) {
            logger.error("Restart Socket Server has error : {1}", e);
        }
    }

    private void closeIO() {
        try {
            if (ois != null) ois.close();
            if (oos != null) oos.close();
        } catch (IOException ex) {
            logger.error("Input/Output Stream has Error : {1}", ex);
        }
    }
}
