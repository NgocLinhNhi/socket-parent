package heaven.sky.project.socket.server;

import heaven.sky.project.socket.config.interfaces.ICommand;
import heaven.sky.project.socket.server.config.ServerComponentIBuildHandler;
import heaven.sky.project.socket.server.command_impl.ChatServer;

public class ServerSocketStartUp {

    //Command Design pattern
    private ICommand command;
    private static ServerSocketStartUp INSTANCE;

    //Singleton declare
    private static ServerSocketStartUp getInstance(ICommand command) {
        if (INSTANCE == null) INSTANCE = new ServerSocketStartUp(command);
        return INSTANCE;
    }

    private ServerSocketStartUp(ICommand command) {
        this.command = command;
    }

    public static void main(String[] args) {
        ICommand chatServerHandler = new ChatServer(ServerComponentIBuildHandler.getInstance());
        getInstance(chatServerHandler).sendMessage();
    }

    //call command of command Design pattern
    private void sendMessage() {
        command.handleMessage();
    }
}
