package heaven.sky.project.socket.config.interfaces;

import java.io.IOException;
import java.net.Socket;

public interface IMessage {

    void handleText();

    void receiveMessage(Socket client) throws IOException;

    void sendMessage(Socket client) throws IOException;

    void handleMessageFromClient() throws IOException, ClassNotFoundException;

    void handleMessage();

    void handleExit();

}
