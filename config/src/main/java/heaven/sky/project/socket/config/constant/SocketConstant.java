package heaven.sky.project.socket.config.constant;

public class SocketConstant {
    public static final String CONTENT = "Content";
    public static final String BUTTON_SEND = "Send";
    public static final String MENU = "Menu";
    public static final String EXIT = "Exit";
    public static final String SERVER_SOCKET = "Server Socket";
    public static final String CLIENT_SOCKET = "Client Socket";

    public static final String SERVER = "Server :";
    public static final String CLIENT = "Client :";

    public static final String ASK = "Do you want to Exit ?";

    public static final int TEXT_FIELD_SIZE = 25;
}
