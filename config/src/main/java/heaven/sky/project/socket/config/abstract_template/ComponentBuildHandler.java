package heaven.sky.project.socket.config.abstract_template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//Template method design pattern
public abstract class ComponentBuildHandler {
    private Logger logger = LoggerFactory.getLogger(ComponentBuildHandler.class);

    public final void handle() {
        try {
            preHandle();
            doHandle();
            postHandle();
        } catch (Exception e) {
            logger.error("handle request: {1}, ", e);
        }
    }

    protected abstract void preHandle() throws Exception;

    protected abstract void doHandle() throws Exception;

    protected abstract void postHandle() throws Exception;

}
