package heaven.sky.project.socket.config.otherway;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

class MyServer {
    public static void main(String[] args) throws Exception {
        // Mở một ServerSocket tại cổng 1687.
        // Chú ý bạn không thể chọn cổng nhỏ hơn 1023 nếu không là người dùng với server
        ServerSocket listener = new ServerSocket(1987);
        // Chấp nhận một yêu cầu kết nối từ phía Client.
        // Đồng thời nhận được một đối tượng Socket tại server.
        Socket client = listener.accept();
        DataInputStream din = new DataInputStream(client.getInputStream());
        DataOutputStream dout = new DataOutputStream(client.getOutputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String str = "", str2 = "";
        while (!str.equals("stop")) {
            str = din.readUTF();
            System.out.println("client says: " + str);
            str2 = br.readLine();
            dout.writeUTF(str2);
            dout.flush();
        }
        din.close();
        client.close();
        listener.close();
    }
}