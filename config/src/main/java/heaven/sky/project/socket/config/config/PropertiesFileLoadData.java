package heaven.sky.project.socket.config.config;

import heaven.sky.project.socket.config.io.InputStreams;
import lombok.Getter;
import lombok.Setter;

import java.util.Properties;

@Getter
@Setter
class PropertiesFileLoadData {

    private static PropertiesFileLoadData INSTANCE;

    static PropertiesFileLoadData getInstance() {
        if (INSTANCE == null) INSTANCE = new PropertiesFileLoadData();
        return INSTANCE;
    }

    Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("application.properties");
        if (file == null)
            file = "application.properties";
        return file;
    }

}
