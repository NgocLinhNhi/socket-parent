package heaven.sky.project.socket.config.interfaces;

public interface IConfig {
    void start() throws Exception;

    void stop() throws Exception;
}
