package heaven.sky.project.socket.config.config;

import heaven.sky.project.socket.config.interfaces.IConfig;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.Socket;

@Getter
@Setter
public class SocketClientConfig implements IConfig {
    private final Logger logger = LoggerFactory.getLogger(SocketClientConfig.class);

    private static SocketClientConfig INSTANCE;
    private Socket communicativeClient;
    private String ip;
    private String port;

    public static SocketClientConfig getInstance() {
        if (INSTANCE == null) INSTANCE = new SocketClientConfig();
        return INSTANCE;
    }

    public void initSocketClientConfig() throws Exception {
        loadProxy();
        start();
    }

    @Override
    public void start() throws Exception {
        // Lấy config Ip từ máy khác => Khi nào cài đặc Server ở host chung / client tại máy của mình
        //communicativeClient = new Socket(ip, Integer.valueOf(port));
        //Mặc định lấy Ip là localhost => Test khi nào cài đặt cả client và Server trên cùng 1 host
        communicativeClient = new Socket(InetAddress.getLocalHost(), Integer.valueOf(port));
        logger.info("Connection to ip {} with port {} has been opened", ip, port);
    }

    @Override
    public void stop() throws Exception {
        communicativeClient.close();
        logger.info("communicative Client has been closed !!!");
    }

    private void loadProxy() throws Exception {
        this.ip = PropertiesFileLoadData.getInstance().loadProperties().getProperty("socket.server.ip");
        this.port = PropertiesFileLoadData.getInstance().loadProperties().getProperty("socket.server.port");
    }


}
