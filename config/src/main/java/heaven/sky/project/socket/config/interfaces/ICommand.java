package heaven.sky.project.socket.config.interfaces;

public interface ICommand {
    void handleMessage();
}
