package heaven.sky.project.socket.config.config;

import heaven.sky.project.socket.config.interfaces.IConfig;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.net.Socket;

@Getter
@Setter
public class SocketServerConfig implements IConfig {
    private final Logger logger = LoggerFactory.getLogger(SocketServerConfig.class);

    private ServerSocket serverSocket;
    private String ip;
    private String port;
    private Socket communicative;

    public static SocketServerConfig getInstance() {
        return new SocketServerConfig();
    }

    private SocketServerConfig() {}

    public void initSocketServerConfig() throws Exception {
        loadProxy();
        start();
    }

    @Override
    public void start() throws Exception {
        logger.info("Open new Socket server with port " + port);
        serverSocket = new ServerSocket(Integer.valueOf(port));
        //Mở 1 client socket để client và server đều read và write message (byte) thông qua object client này
        logger.info("new connection Socket client has been opened");
        communicative = serverSocket.accept();

    }

    @Override
    public void stop() throws Exception {
        serverSocket.close();
        communicative.close();
        logger.info("communicative and Server Socket has been closed !!!");
    }

    private void loadProxy() throws Exception {
        this.port = PropertiesFileLoadData.getInstance().loadProperties().getProperty("socket.server.port");
    }
}
