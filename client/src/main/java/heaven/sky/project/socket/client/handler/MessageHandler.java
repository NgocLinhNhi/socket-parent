package heaven.sky.project.socket.client.handler;

import heaven.sky.project.socket.client.config.InitClientSwing;
import heaven.sky.project.socket.config.config.SocketClientConfig;
import heaven.sky.project.socket.config.interfaces.IMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static heaven.sky.project.socket.config.constant.SocketConstant.*;

public class MessageHandler implements IMessage {
    private final Logger logger = LoggerFactory.getLogger(MessageHandler.class);

    private ObjectInputStream ois = null;
    private ObjectOutputStream oos = null;
    private String port;

    private SocketClientConfig instance = SocketClientConfig.getInstance();

    public static MessageHandler getInstance() {
        return new MessageHandler();
    }

    @Override
    public void handleText() {
        InitClientSwing instance = InitClientSwing.getInstance();
        try {
            String data = instance.getTextField().getText();
            oos.writeObject(data);

            instance.getTxArea().append(CLIENT + data + "\r\n");
            instance.getTextField().setText(null);
        } catch (IOException ex) {
            logger.error("Handle Text has error : {1}", ex);
        }
    }

    @Override
    public void receiveMessage(Socket communicative) throws IOException {
        ois = new ObjectInputStream(communicative.getInputStream());
    }

    @Override
    public void sendMessage(Socket communicative) throws IOException {
        oos = new ObjectOutputStream(communicative.getOutputStream());
        oos.writeObject("Socked Client connected to Server with port : " + port);
        oos.flush();
    }

    @Override
    public void handleMessageFromClient() throws IOException, ClassNotFoundException {
        while (true) {
            String data = (String) ois.readObject();
            InitClientSwing.getInstance().getTxArea().append(SERVER + data + "\r\n");
        }
    }

    @Override
    public void handleMessage() {
        try {
            Socket communicative = getConnection();

            sendMessage(communicative);
            receiveMessage(communicative);
            handleMessageFromClient();
        } catch (Exception ex) {
            //Vì SOcket là 1 giao thức mạng 2 chiều song song -> chỉ cần 1 client/server close => bên còn lại sẽ nhận được ngay error
            //java.net.SocketException: Connection reset => 1 lỗi khá nỗi tiềng của socket
            //=> Với Client khi Server Closed => nên close luôn Client là ok .
            closeSocketClientReport();
            // Không cần catch Exception ở đây => Stop trước catch Exception là được
            //logger.error("handle Message Client has error {1}",ex);
        } finally {
            closeIO();
            logger.info("File I/O has been closed");
            reportWarning();
        }
    }

    @Override
    public void handleExit() {
        int flag = JOptionPane.showConfirmDialog(null, ASK);
        if (flag == JOptionPane.YES_OPTION) System.exit(0);
    }

    private Socket getConnection() throws Exception {
        instance.initSocketClientConfig();
        this.port = instance.getPort();
        return instance.getCommunicativeClient();
    }

    private void reportWarning() {
        JOptionPane.showMessageDialog(
                null,
                "Socket Server has been closed !!!",
                "Warning",
                JOptionPane.WARNING_MESSAGE);
        //Close all swing form
        System.exit(0);
    }

    private void closeSocketClientReport() {
        try {
            logger.info("Socked Server has been closed !!! => Auto closed Socket Client");
            instance.stop();
        } catch (Exception e) {
            logger.error("Close Socket Clien has error {1}", e);
        }
    }

    private void closeIO() {
        try {
            if (ois != null) ois.close();
            if (oos != null) oos.close();
        } catch (IOException ex) {
            logger.error("Input/Output Stream has Error : {1}", ex);
        }
    }
}
