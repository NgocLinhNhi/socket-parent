package heaven.sky.project.socket.client;

import heaven.sky.project.socket.client.command_impl.ChatClient;
import heaven.sky.project.socket.client.config.ClientComponentBuildHandler;
import heaven.sky.project.socket.config.interfaces.ICommand;

public class ClientSocketStartUp {

    //Command Design pattern
    private ICommand command;
    private static ClientSocketStartUp INSTANCE;

    //Singleton declare
    private static ClientSocketStartUp getInstance(ICommand command) {
        if (INSTANCE == null) INSTANCE = new ClientSocketStartUp(command);
        return INSTANCE;
    }

    private ClientSocketStartUp(ICommand command) {
        this.command = command;
    }

    public static void main(String[] args) {
        ICommand chatClientHandler = new ChatClient(ClientComponentBuildHandler.getInstance());
        getInstance(chatClientHandler).sendMessage();
    }

    //call command of command Design pattern
    private void sendMessage() {
        command.handleMessage();
    }
}
