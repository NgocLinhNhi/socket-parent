package heaven.sky.project.socket.client.config;

import heaven.sky.project.socket.config.abstract_template.ComponentBuildHandler;

//Template method design pattern
public class ClientComponentBuildHandler extends ComponentBuildHandler {

    private static ClientComponentBuildHandler INSTANCE;

    public static ClientComponentBuildHandler getInstance() {
        if (INSTANCE == null) INSTANCE = new ClientComponentBuildHandler();
        return INSTANCE;
    }

    @Override
    protected void preHandle() {
        InitClientSwing.getInstance().buildForm();
    }

    @Override
    protected void doHandle() {
        InitClientSwing.getInstance().addEventFunction();
    }

    @Override
    protected void postHandle() {
        InitClientSwing.getInstance().setSizeSwing();
    }
}
