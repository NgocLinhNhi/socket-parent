package heaven.sky.project.socket.client.command_impl;

import heaven.sky.project.socket.config.interfaces.ICommand;
import heaven.sky.project.socket.client.config.ClientComponentBuildHandler;

//Command Design pattern call Template method như 1 Interface mô tả business
public class ChatClient implements ICommand {

    private ClientComponentBuildHandler buildComponent;

    public ChatClient(ClientComponentBuildHandler buildComponent) {
        this.buildComponent = buildComponent;
    }

    @Override
    public void handleMessage() {
        buildComponent.handle();
    }

}
