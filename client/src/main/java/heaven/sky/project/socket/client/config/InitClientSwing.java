package heaven.sky.project.socket.client.config;

import heaven.sky.project.socket.client.handler.MessageHandler;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;

import static heaven.sky.project.socket.config.constant.SocketConstant.*;

@Getter
@Setter
public class InitClientSwing extends JFrame {
    private static InitClientSwing INSTANCE;

    JMenuItem menuExit;
    JButton btSend;
    JTextField textField;
    JTextArea txArea;
    MessageHandler messageHandler = MessageHandler.getInstance();

    public static InitClientSwing getInstance() {
        if (INSTANCE == null) INSTANCE = new InitClientSwing();
        return INSTANCE;
    }

    void buildForm() {
        JLabel label = new JLabel(CONTENT);
        textField = new JTextField(TEXT_FIELD_SIZE);
        txArea = new JTextArea();
//        txArea.setEnabled(false);
//        txArea.setDisabledTextColor(Color.black);
        btSend = new JButton(BUTTON_SEND);

        setLayout(new BorderLayout());

        JPanel panelNorth = new JPanel(new FlowLayout());
        panelNorth.add(label);
        panelNorth.add(textField);
        panelNorth.add(btSend);

        JMenu menu = new JMenu(MENU);
        menuExit = new JMenuItem(EXIT);
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menu);
        menu.addSeparator();
        menu.add(menuExit);
        setJMenuBar(menuBar);

        JScrollPane jScrollPane = new JScrollPane(txArea);
        Container con = getContentPane();
        con.add(panelNorth, BorderLayout.NORTH);
        con.add(jScrollPane, BorderLayout.CENTER);
    }

    void addEventFunction() {
        addExitFunction();
        addSendButtonFunction();
        addSendTextFunction();
    }

    void setSizeSwing() {
        setTitle(CLIENT_SOCKET);
        setSize(450, 450);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        handleMessage();
    }

    //Add Event cho Button Exit
    private void addExitFunction() {
        menuExit.addActionListener(e -> messageHandler.handleExit());
    }

    //Add Event cho Button Send
    private void addSendButtonFunction() {
        btSend.addActionListener(e -> messageHandler.handleText());
    }

    //Add Event cho TextField
    private void addSendTextFunction() {
        textField.addActionListener(e -> messageHandler.handleText());
    }

    private void handleMessage() {
        messageHandler.handleMessage();
    }
}
